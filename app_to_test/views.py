from django.contrib.auth.models import User
from rest_framework import viewsets

from .serializers import UserSerializer

"""
API Endpoint Views
"""


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint for users CRUD.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    lookup_field = 'username'
