from django.apps import AppConfig


class AppToTestConfig(AppConfig):
    name = 'app_to_test'
