## Table of Contents

- [Installation](#installation)
- [Assessment](#assessment)
- [Hint](#hint)

## Installation

You can clone this project in your development environment.

You can then install all the needed packages with this command inside your project:

```pip install -r requirements.txt```

To create the database, run this command :

```python manage.py migrate```

Now you can run your django server : 

```python manage.py runserver```

Go to this url http://127.0.0.1:8000/api/users/ to see if everything up.

## Assessment 

The goal is to **create unit tests.**

In the app_to_test module you will need to **test the different methods to create, read the users and update, delete a user.**

Have a great coding time ! 


## Hints

We are using the **django-rest-framework**.

You should look in that library for the tests set up.

